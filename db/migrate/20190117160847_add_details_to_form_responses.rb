class AddDetailsToFormResponses < ActiveRecord::Migration[5.2]
  def change
    add_column :form_responses, :order_phone_number, :string
    add_column :form_responses, :order_date, :datetime
  end
end
