class CreateFormResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :form_responses do |t|
      t.text :response_body

      t.timestamps
    end
  end
end
