class FormsController < ApplicationController

  def submit
    begin
      @row_record = params[:record].split(',')

      order =  FormResponse.where(order_phone_number:  @row_record[2], order_date:  @row_record[4].to_time)
      if order.blank?
          require 'rubygems'
          require 'mechanize'
          agent = Mechanize.new
          page = agent.get('https://www.szamlazz.hu/szamla/login')
          page.form.usrloginname = "zsombortnagy@gmail.com"
          page.form.usrpassword = "abc"
          page.form.submit
          page = agent.get('https://www.szamlazz.hu/szamla/?page=szamlaszerkeszto')

          form = page.form_with(:id => 'form_newszla')

          gross_unit_price = @row_record[6].delete(' ').tr('(Ft)', '').to_i
          gross_price = 1 * gross_unit_price
          deduction_vat_rate = ((gross_price.to_f/118) * 18).round
          net_price = (gross_price - deduction_vat_rate).to_s

          form.partnername = @row_record[1]
          form.partneraddr2 = @row_record[3]
          form.fizmod = @row_record[5]

          form.megjegyzes = @row_record[2].delete(' ') + " "+ @row_record[4] + " "+ @row_record[0]

          form.nettegysar_1 = net_price

          form.maxrownum = "2"
          form.partnerirsz = "1111"
          form.partnercity = "Budapest"
          form.item_1 = "Pizza";
          form.menny_1 = "1";
          form.meegys_1 = "db";
          form.roworder_1 = "0"

          form.nettar_1 = net_price
          form.brutar_1 = gross_price.to_s
          form.brutegysar_1 = gross_unit_price.to_s

          form.afa_1 = deduction_vat_rate.to_s
          form.nettartotal = net_price
          form.afatotal = deduction_vat_rate.to_s
          form.brutartotal = gross_unit_price.to_s

          #result = agent.submit(form)

          result = form.submit
          response = result.body.force_encoding("UTF-8")

          FormResponse.create(response_body: response, order_phone_number:  @row_record[2], order_date:  @row_record[4].to_time)
          render status: :ok , json: {message: "form submit successfully"}
      else
        render status: 201 , json: {message: "record already exist"}
      end
    rescue => e
      render json: e.message, status: :bad_request
    end
  end

end
